#include <ilcplex/ilocplex.h>

ILOSTLBEGIN

int main(int argc, char **argv) 
{
	IloEnv env;
	try 
	{
		int n, pesoMax;
		cin >> n;
		cin >> pesoMax;

		int* valor;
		int* peso;
		valor = new int[n];		
		peso = new int[n];
		
		for(int i=0 ; i<n; i++)
			cin >> valor[i];

		
		for(int i=0 ; i<n; i++)
			cin >> peso[i];


		// create model
		IloModel mochila(env, "Problema da Mochila");
		IloCplex cplex(mochila);

		// Criando as variáveis binárias
		IloIntVarArray x(env, n, 0, 1); 

		// Função Objetivo
		IloExpr obj(env);
		for (int i = 0; i < n; i++)
			obj += valor[i] * x[i];
		mochila.add(IloMaximize(env, obj));

		// Restrição Peso
		IloExpr somatorio(env);
		for (int i = 0; i < n; i++)
			somatorio += peso[i] * x[i];
		mochila.add(somatorio <= pesoMax);


		if ( cplex.solve() )
			env.out() << "Valor Ótimo "
					<< cplex.getObjValue() << endl;

		IloNumArray sol(env,  n);
		cplex.getValues(sol, x);

		int pesoTotal = 0;
		for(int i = 0; i < n; i++)
			if(sol[i] == 1)
				pesoTotal += peso[i];
		cout << "Peso total = " << pesoTotal << endl;

		// Imprimindo a solução
		for(int i = 0; i < n; i++)
			cout << "Objeto " 
					<< i+1 << ": valor = " << valor[i] << " peso = " 
					<< peso[i] << " está na solução -> " << sol[i] << endl;
	}
	catch (const IloException& e) 
	{
		cerr << "Exception caught: " << e << endl;
	}
	catch (...)
	{
		cerr << "Unknown exception caught!" << endl;
	}

	env.end();
	return 0;
}

