#include <ilcplex/ilocplex.h>

ILOSTLBEGIN

int main(int argc, char **argv)
{

    int tipos_racoes = 2;
    int quantidade_cereais = 30000;
    int quantidade_carnes = 10000;

    // Criando o ambiente
	IloEnv env;
    try
    {
        // Modelo
		IloModel racao(env, "Problema da Ração de Cachorro");

		// Inicializando o objeto cplex
		IloCplex cplex(racao);

		// Variável de decisão
        IloNumVarArray x(env, tipos_racoes, 0, IloInfinity);

		// Função Objetivo
		IloExpr obj(env);
		obj = 11 * x[0] + 12 * x[1];

		racao.add(IloMaximize(env,obj));

		//Restriçoẽs
		IloExpr restricao_carne(env);
		restricao_carne = x[0] + 4 * x[1];
		racao.add(restricao_carne <= quantidade_carnes);

		IloExpr restricao_cereal(env);
		restricao_cereal = 5 * x[0] + 2 * x[1];
		racao.add(restricao_cereal <= quantidade_cereais);

		// Rodando o algoritmo
		if ( cplex.solve() )
			cout << "Custo ótimo "
					<< cplex.getObjValue() << endl;

		// Obtendo a solução
		IloNumArray sol(env,  tipos_racoes);
		cplex.getValues(sol, x);

		// Imprimindo a solução
		for(int i = 0; i < tipos_racoes; i++)
			cout << "Quantidade produzida da ração "
					<< i+1 << ": " << sol[i] << endl;
    }
	catch (const IloException& e)
	{
		cerr << "Exception caught: " << e << endl;
	}
	catch (...)
	{
		cerr << "Unknown exception caught!" << endl;
	}

	env.end();
	return 0;
}
